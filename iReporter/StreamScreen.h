//
//  StreamScreen.h
//  iReporter
//
//  Created by Marin Todorov on 10/02/2012.
//  Copyright (c) 2012 Marin Todorov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoView.h"

@interface StreamScreen : UIViewController <PhotoViewDelegate> {
    IBOutlet UIBarButtonItem* btnCompose;
    IBOutlet UIBarButtonItem* btnRefresh;
    IBOutlet UIScrollView* listView;
    IBOutlet UILabel* scoreLabel;
    IBOutlet UIButton* quitButton;
    int count;
}
@property (strong, nonatomic) NSNumber* score;
@property (strong, nonatomic) NSString* ImageNumber;

//refresh the photo stream
-(IBAction)btnRefreshTapped;
-(IBAction)quitGame;


@end
