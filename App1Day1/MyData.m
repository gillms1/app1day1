//
//  MyData.m
//  App1Day1
//
//  Created by Manprit Gill on 03/07/2014.
//  Copyright (c) 2014 Marin Todorov. All rights reserved.
//

#import "MyData.h"

@implementation MyData

@synthesize myCount;
@synthesize imageGuessed;
@synthesize imageGuessedCorrectly;


+(MyData*) sharedInstance {
    
    static MyData *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc]init];
       
    });
    return instance;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.myCount = 0;
       
    }
    return self;
}


-(void) score: (NSString*)imageNumber withAnswer: (BOOL *)isCorrect{
    if (imageGuessed == nil){
        imageGuessed = [[NSMutableArray alloc]init];
        imageGuessedCorrectly = [[NSMutableArray alloc]init];

    }
    
     if (![imageGuessed containsObject:imageNumber]){
        if (isCorrect){
            myCount++;
        }else{
            myCount--;
        }
         [imageGuessed addObject:imageNumber];
         [imageGuessedCorrectly addObject:imageNumber];
    }

}


/*
-(void) initialiseGuessesAndScores{
    imageGuessed = [NSMutableArray array];
    imageGuessedCorrectly =[NSMutableArray array];
    for (int i = 0; i < 10; i++){
        [imageGuessed arrayByAddingObject:[NSNumber numberWithBool:NO]];
        [imageGuessedCorrectly arrayByAddingObject:[NSNumber numberWithBool:NO]];
       
    }
}




-(void) score: (int)imageNumber withAnswer: (bool)isCorrect{
     if (isCorrect == YES){
        if([[imageGuessed objectAtIndex:imageNumber] boolValue]==NO)
        {
            myCount ++;
            [imageGuessedCorrectly replaceObjectAtIndex:imageNumber withObject:[NSNumber numberWithBool:YES]] ;
        }
    }else{
        if([[imageGuessed objectAtIndex:imageNumber] boolValue]==NO)
        {
            myCount --;
            [imageGuessedCorrectly replaceObjectAtIndex:imageNumber withObject:[NSNumber numberWithBool:NO]] ;
        }
    }
    [imageGuessed replaceObjectAtIndex:imageNumber withObject:[NSNumber numberWithBool:YES]];
}
*/



@end

